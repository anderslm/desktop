Upstream: yes, cherry-picked from d256d9055095f27a33696e0aeda4ee20ed4fb1a0

From 7ecfc433882b3700995a0b0a1312e761f4ebb0bd Mon Sep 17 00:00:00 2001
From: Reiner Herrmann <reiner@reiner-h.de>
Date: Fri, 17 Apr 2020 19:25:40 +0200
Subject: [PATCH] Move variable declarations from header to C file to fix build
 with GCC 10

GCC 10 builds with -fno-common by default, which causes linker errors when
variables are declared in header files and included in multiple places.

See also: https://gcc.gnu.org/gcc-10/porting_to.html
---
 common/lualib.c  | 2 ++
 common/lualib.h  | 2 +-
 luaa.c           | 2 ++
 luaa.h           | 2 +-
 objects/button.c | 2 ++
 objects/button.h | 2 +-
 objects/client.c | 2 ++
 objects/client.h | 2 +-
 objects/drawin.c | 2 ++
 objects/drawin.h | 2 +-
 objects/key.c    | 2 ++
 objects/key.h    | 2 +-
 objects/tag.c    | 2 ++
 objects/tag.h    | 2 +-
 objects/window.c | 1 +
 objects/window.h | 2 +-
 16 files changed, 23 insertions(+), 8 deletions(-)

diff --git a/common/lualib.c b/common/lualib.c
index 312fb2d5..bb110ac7 100644
--- a/common/lualib.c
+++ b/common/lualib.c
@@ -20,6 +20,8 @@
 #include "common/lualib.h"
 #include "luaa.h"
 
+lua_CFunction lualib_dofunction_on_error;
+
 void luaA_checkfunction(lua_State *L, int idx)
 {
     if(!lua_isfunction(L, idx))
diff --git a/common/lualib.h b/common/lualib.h
index 8a3ef089..a9126184 100644
--- a/common/lualib.h
+++ b/common/lualib.h
@@ -28,7 +28,7 @@
 #include "common/util.h"
 
 /** Lua function to call on dofunction() error */
-lua_CFunction lualib_dofunction_on_error;
+extern lua_CFunction lualib_dofunction_on_error;
 
 void luaA_checkfunction(lua_State *, int);
 void luaA_checktable(lua_State *, int);
diff --git a/luaa.c b/luaa.c
index bfe8eee3..aae05cd4 100644
--- a/luaa.c
+++ b/luaa.c
@@ -77,6 +77,8 @@ extern const struct luaL_Reg awesome_root_lib[];
 extern const struct luaL_Reg awesome_mouse_methods[];
 extern const struct luaL_Reg awesome_mouse_meta[];
 
+signal_array_t global_signals;
+
 /** A call into the Lua code aborted with an error.
  *
  * This signal is used in the example configuration, @{05-awesomerc.md},
diff --git a/luaa.h b/luaa.h
index 6f6f34f1..f8f83ebe 100644
--- a/luaa.h
+++ b/luaa.h
@@ -311,7 +311,7 @@ const char *luaA_find_config(xdgHandle *, const char *, luaA_config_callback *);
 bool luaA_parserc(xdgHandle *, const char *);
 
 /** Global signals */
-signal_array_t global_signals;
+extern signal_array_t global_signals;
 
 int luaA_class_index_miss_property(lua_State *, lua_object_t *);
 int luaA_class_newindex_miss_property(lua_State *, lua_object_t *);
diff --git a/objects/button.c b/objects/button.c
index 846ed7d4..36cfe696 100644
--- a/objects/button.c
+++ b/objects/button.c
@@ -35,6 +35,8 @@
 
 #include "button.h"
 
+lua_class_t button_class;
+
 /** Button object.
  *
  * @tfield int button The mouse button number, or 0 for any button.
diff --git a/objects/button.h b/objects/button.h
index fb8bb8da..8f0b8943 100644
--- a/objects/button.h
+++ b/objects/button.h
@@ -39,7 +39,7 @@ typedef struct button_t
     xcb_button_t button;
 } button_t;
 
-lua_class_t button_class;
+extern lua_class_t button_class;
 LUA_OBJECT_FUNCS(button_class, button_t, button)
 ARRAY_FUNCS(button_t *, button, DO_NOTHING)
 
diff --git a/objects/client.c b/objects/client.c
index 8541b415..500d0426 100644
--- a/objects/client.c
+++ b/objects/client.c
@@ -106,6 +106,8 @@
 #include <xcb/shape.h>
 #include <cairo-xcb.h>
 
+lua_class_t client_class;
+
 /** Client class.
  *
  * This table allow to add more dynamic properties to the clients. For example,
diff --git a/objects/client.h b/objects/client.h
index 45bc9338..695db84d 100644
--- a/objects/client.h
+++ b/objects/client.h
@@ -192,7 +192,7 @@ struct client_t
 ARRAY_FUNCS(client_t *, client, DO_NOTHING)
 
 /** Client class */
-lua_class_t client_class;
+extern lua_class_t client_class;
 
 LUA_OBJECT_FUNCS(client_class, client_t, client)
 
diff --git a/objects/drawin.c b/objects/drawin.c
index 3fd1cc43..3bbd9317 100644
--- a/objects/drawin.c
+++ b/objects/drawin.c
@@ -46,6 +46,8 @@
 #include <cairo-xcb.h>
 #include <xcb/shape.h>
 
+lua_class_t drawin_class;
+
 /** Drawin object.
  *
  * @field border_width Border width.
diff --git a/objects/drawin.h b/objects/drawin.h
index 31f315aa..2f8887d9 100644
--- a/objects/drawin.h
+++ b/objects/drawin.h
@@ -52,7 +52,7 @@ void luaA_drawin_systray_kickout(lua_State *);
 
 void drawin_class_setup(lua_State *);
 
-lua_class_t drawin_class;
+extern lua_class_t drawin_class;
 
 #endif
 // vim: filetype=c:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
diff --git a/objects/key.c b/objects/key.c
index 932921ca..b1c65f82 100644
--- a/objects/key.c
+++ b/objects/key.c
@@ -43,6 +43,8 @@
 #include <xkbcommon/xkbcommon.h>
 #include <glib.h>
 
+lua_class_t key_class;
+
 /** Key object.
  *
  * @tfield string key The key to trigger an event.
diff --git a/objects/key.h b/objects/key.h
index 8aa07ffe..45f23cac 100644
--- a/objects/key.h
+++ b/objects/key.h
@@ -36,7 +36,7 @@ typedef struct keyb_t
     xcb_keycode_t keycode;
 } keyb_t;
 
-lua_class_t key_class;
+extern lua_class_t key_class;
 LUA_OBJECT_FUNCS(key_class, keyb_t, key)
 DO_ARRAY(keyb_t *, key, DO_NOTHING)
 
diff --git a/objects/tag.c b/objects/tag.c
index 9b289dee..d68553bd 100644
--- a/objects/tag.c
+++ b/objects/tag.c
@@ -190,6 +190,8 @@
 #include "ewmh.h"
 #include "luaa.h"
 
+lua_class_t tag_class;
+
 /**
  * @signal request::select
  */
diff --git a/objects/tag.h b/objects/tag.h
index d6bb40e0..f830b1e2 100644
--- a/objects/tag.h
+++ b/objects/tag.h
@@ -46,7 +46,7 @@ struct tag
     client_array_t clients;
 };
 
-lua_class_t tag_class;
+extern lua_class_t tag_class;
 LUA_OBJECT_FUNCS(tag_class, tag_t, tag)
 
 void tag_class_setup(lua_State *);
diff --git a/objects/window.c b/objects/window.c
index 3c043273..16c57f87 100644
--- a/objects/window.c
+++ b/objects/window.c
@@ -59,6 +59,7 @@
 #include "property.h"
 #include "xwindow.h"
 
+lua_class_t window_class;
 LUA_CLASS_FUNCS(window, window_class)
 
 static xcb_window_t
diff --git a/objects/window.h b/objects/window.h
index 5386fafe..fbc5296c 100644
--- a/objects/window.h
+++ b/objects/window.h
@@ -80,7 +80,7 @@ typedef struct
     WINDOW_OBJECT_HEADER
 } window_t;
 
-lua_class_t window_class;
+extern lua_class_t window_class;
 
 void window_class_setup(lua_State *);
 
-- 
2.26.2

