# Copyright 2008, 2009 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require pam \
    autotools [ supported_autoconf=[ 2.5 ]  supported_automake=[ none ] ]

SUMMARY="Modular X screen saver and locker"
DESCRIPTION="
XScreenSaver is the standard screen saver collection shipped on most
Linux and Unix systems running the X11 Window System.
"
HOMEPAGE="https://www.jwz.org/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/faq.html"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    caps
    gtk [[ description = [ Build the preferences GUI and some additional screen savers ] ]]
    opengl
    pam
    systemd
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: da de es et fi fr hu it ja ko nb nl pl pt pt_BR ru sk sv vi wa zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/atk
        dev-libs/libxml2:2.0
        media-libs/libpng:=
        sys-apps/bc
        x11-apps/xdg-utils
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXft[>=2.1.0]
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXmu
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libXxf86misc
        x11-libs/libXxf86vm
        caps? ( sys-libs/libcap )
        gtk? (
            dev-libs/glib:2
            gnome-platform/libglade:2
            x11-libs/gdk-pixbuf:2.0[X]
            x11-libs/gtk+:2[>=2.22]
            x11-libs/pango
        )
        opengl? (
            dev-libs/libglvnd
            x11-dri/glu
            x11-dri/mesa
        )
        pam? ( sys-libs/pam )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        systemd? ( sys-apps/systemd[>=221] )
    recommendation:
        x11-apps/appres [[ description = [ needed for xscreensaver-text ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/fix_configure.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-locking
    --enable-nls
    --disable-root-passwd
    --with-dpms-ext
    --with-browser=xdg-open
    --with-jpeg
    --with-png
    --with-proc-interrupts
    --with-pthread
    --with-randr-ext
    --with-x-app-defaults=/usr/share/X11/app-defaults
    --with-xdbe-ext
    --with-xf86gamma-ext
    --with-xf86vmode-ext
    --with-xft
    --with-xinerama-ext
    --with-xinput-ext
    --with-xshm-ext
    --without-gle
    --without-gles
    --without-kerberos
    --without-readdisplay
    --without-setuid-hacks
    --without-motif
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'caps setcap-hacks'
    gtk
    'gtk pixbuf'
    'gtk record-animation'
    'opengl gl'
    pam
    systemd
)

src_prepare() {
    default

    # install xscreensaver-systemd binary when requested
    # last checked: 5.43
    edo sed \
        -e 's:@EXES_OSX@ ;:@EXES_OSX@ @EXES_SYSTEMD@ ;:g' \
        -i driver/Makefile.in

    # Makefile.am exists, so aclocal does not fail. Automake won't succeed, so no point in running aclocal
    eautoconf
}

src_install() {
    emake install_prefix="${IMAGE}" install

    if option pam ; then
        pamd_mimic_system "${PN}" auth auth
    fi

    # do not install man pages for nonexistent binaries
    ! option gtk && edo rm "${IMAGE}"/usr/share/man/man1/${PN}-demo.1
    ! option systemd && edo rm "${IMAGE}"/usr/share/man/man1/${PN}-systemd.1
}

pkg_postinst() {
    if option caps ; then
        edo setcap cap_net_raw=p /usr/$(exhost --target)/libexec/xscreensaver/sonar
    fi
}

