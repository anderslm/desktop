# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.12 1.10 ] ]
require option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="An asynchronous XMLPP library"
HOMEPAGE="http://www.loudmouth-project.org/"
DOWNLOADS="http://engineyard.github.com/loudmouth/download/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.12.0]
        dev-libs/libtasn1[>=0.2.6]
        dev-libs/libxml2:2.0
        net-dns/libidn
        net-libs/libasyncns[>=0.3]
        providers:gnutls? ( dev-libs/gnutls[>=1.4.0] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    build:
        dev-libs/check[>=0.9.2]
        virtual/pkg-config[>=0.20]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PNV}-Applied-parallel-make-and-asyncns-patches-from-the-m.patch"
    "${FILES}/${PNV}-Use-pkg-config-to-detect-GnuTLS-for-compatibility-wi.patch"
    "${FILES}/${PNV}-disable-invalid-tests-test.patch"
    "${FILES}/${PNV}-fix-glib-2.32-include.patch"
)

src_configure() {
    local ssl='openssl'

    option providers:gnutls && ssl='gnutls'

    econf --with-asyncns=system --with-ssl=${ssl}
}

