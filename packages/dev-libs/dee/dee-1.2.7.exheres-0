# Copyright 2016-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ branch=1.0 ] \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="Library using DBus to provide objects"
DESCRIPTION="
Libdee is a library that uses DBus to provide objects allowing you to create Model-View-Controller
type programs across DBus. It also consists of utility objects which extend DBus allowing for
peer-to-peer discoverability of known objects without needing a central registrar.
"

LICENCES="LGPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="gobject-introspection"

# tests require buried dbus-test-runner package
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        dev-libs/glib:2[>=2.38]
        dev-libs/icu:=[>=4.6]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.10.2] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
     "${FILES}"/${PN}-1.2.7-gcc6-fixes.patch
     "${FILES}"/${PN}-1.2.7-deprecated-g_type_class_add_private.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-icu
    --disable-extended-tests
    --disable-gcov
    --disable-static
    --disable-trace-log
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'gobject-introspection introspection' )
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

