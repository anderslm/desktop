# Copyright 2014 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="Objects and helper methods to help reading and writing AppStream metadata"
HOMEPAGE="https://people.freedesktop.org/~hughsient/${PN} https://github.com/hughsie/${PN}"
DOWNLOADS="https://people.freedesktop.org/~hughsient/${PN}/releases/${PNV}.tar.xz"

REMOTE_IDS="github:hughsie/${PN}"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt [[ note = [ For xsltproc ] ]]
        dev-util/gperf
        sys-devel/gettext[>=0.19.7]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.9] )
    build+run:
        app-arch/libarchive
        core/json-glib[>=1.1.2]
        dev-libs/glib:2[>=2.45.8]
        gnome-desktop/libsoup:2.4[>=2.51.92]
        x11-libs/gdk-pixbuf:2.0[>=2.31.5][gobject-introspection?]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.8] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-as-app-properly-initialize-unique_id_mutex.patch
    "${FILES}"/${PNV}-trivial-Fix-CI-by-moving-future-back-a-bit.patch
)
MESON_SRC_CONFIGURE_PARAMS=(
    -Dalpm=false
    -Dbuilder=false
    -Ddep11=false
    -Dfonts=false
    -Dman=true
    -Drpm=false
    -Dstemmer=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gobject-introspection introspection'
    gtk-doc
)

