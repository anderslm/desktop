# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lathiat release=v${PV} suffix=tar.gz ]

SUMMARY="Name Service Switch module for Multicast DNS"
DESCRIPTION="
nss-mdns is a plugin for the GNU Name Service Switch (NSS) functionality of
the GNU C Library (glibc) providing host name resolution via Multicast DNS
(aka Zeroconf, aka Apple Rendezvous, aka Apple Bonjour), effectively allowing
name resolution by common Unix\/Linux programs in the ad-hoc mDNS domain .local.

nss-mdns provides client functionality only, which means that you have to
run a mDNS responder daemon separately from nss-mdns if you want to register
the local host name via mDNS (e.g. Avahi).
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    run:
        net-dns/avahi
    test:
        dev-libs/check[>=0.11]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    AVAHI_SOCKET="/run/avahi-daemon/socket"
    MDNS_ALLOW_FILE="/etc/mdns.allow"
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

src_install() {
    default

    insinto /etc
    doins "${FILES}"/mdns.allow
}

pkg_postinst() {
    elog "In order to activate one of the NSS modules and be able to resolve"
    elog "hosts from the .local domain you need to edit /etc/nsswitch.conf."
    elog "Please consult the README for details."
}

